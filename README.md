# Cycle Native Alert

**A Cycle.js Driver for creating [Alert](https://facebook.github.io/react-native/docs/0.55/alert) modals in React Native**

```
npm install cycle-native-alert
```

## Usage

### Sink

Stream of command objects, to cause a React Native alert to appear. A command object has the following fields:

- `title`: (required) string, displays as the Alert's title
- `message`: (optional) string, displays as the Alert's textual content
- `buttons`: (optional) array of objects, describes each button in the Alert
  - `text`: (required) string, as the button's label
  - `id`: (required) string, to uniquely identify this button
  - `style`: (optional) string, either 'default' or 'cancel' or 'destructive'
- `options`: (optional) object with field `cancelable` boolean
- `type`: (optional)

### Source

Stream of button ids (strings) to indicate which button id was pressed.

## License

Copyright (C) 2018 Andre 'Staltz' Medeiros, licensed under MIT license

