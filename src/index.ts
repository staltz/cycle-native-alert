import xs, {Stream} from 'xstream';
import {Alert} from 'react-native';

export type ButtonConfig = {
  text: string;
  id: ButtonId;
  style?: 'default' | 'cancel' | 'destructive';
};

export type Command = {
  title: string;
  message?: string;
  buttons?: Array<ButtonConfig>;
  options?: {cancelable: boolean};
  type?: any;
};

export type ButtonId = string;

export function alertDriver(command$: Stream<Command>): Stream<ButtonId> {
  const response$ = xs.create<ButtonId>();

  command$.addListener({
    next: command => {
      Alert.alert(
        command.title,
        command.message,
        (command.buttons || []).map(btn => ({
          text: btn.text,
          style: btn.style,
          onPress: () => {
            response$._n(btn.id);
          },
        })),
      );
    },
  });

  return response$;
}
